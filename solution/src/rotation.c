//
// Created by fk724s on 25.10.2023.
//

#include "rotation.h"
#include "bmp_utils.h"
#include "image_utils.h"

struct image* rotate_image ( const struct image* image, enum direction direction ) {
    struct image* new_image = create_image( image->height, image->width );

    for (uint64_t y = 0; y < ( image->height ); y++) {
        for (uint64_t x = 0; x < ( image->width ); x++) {

            size_t old_pos = x + y * ( image->width);
            size_t new_pos = 0;

            if ( direction == LEFT) {
                new_pos = y + ( ( new_image->height ) - x - 1 ) * ( new_image->width );
            }

            if ( direction == RIGHT ) {
                new_pos = ( ( new_image->width ) - y - 1 ) + x * ( new_image->width );
            }

            ( new_image->data )[ new_pos ] = ( image->data )[ old_pos ];
        }
    }

    return new_image;
}
