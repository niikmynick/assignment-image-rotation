#include "file_utils.h"
#include "bmp_utils.h"
#include "image_utils.h"
#include "rotation.h"

#include <stdio.h>

int main( int argc, char** argv ) {

    if ( argc != 3 ) {
        fprintf(stderr, "Expected 3 arguments, but given %d \n", argc);
        return 1;
    }

    FILE* r_file = open_file( argv[1], read_bytes );
    if ( !r_file ) return 1;


    struct image* source_image;
    enum r_status read_result = read_bmp( r_file, &source_image );
    fclose( r_file );

    if ( read_result != R_OK ) {
        fprintf(stderr, "ImageError: source image has %s\n", string_codes[read_result]);
        if ( source_image ) {
            destroy_image( source_image );
        }
        return 1;
    }
    fprintf(stdout, "[+] Get image from file \n");


    struct image* rotated_image = rotate_image( source_image, RIGHT);
    if ( !rotated_image ) {
        fprintf(stderr, "ProcessError: failed to rotate the image \n");
        destroy_image( source_image );
        return 1;
    }
    fprintf(stdout, "[+] Rotate image \n");


    FILE* w_file = open_file( argv[2], write_bytes );
    if ( !w_file ) {
        destroy_image( source_image );
        destroy_image( rotated_image );
        return 1;
    }

    enum w_status write_result = write_bmp( rotated_image, w_file );
    fclose( w_file );

    if ( write_result != W_OK ) {
        fprintf(stderr, "FileError: an error occurred when writing to the file: %s \n", argv[2]);
        destroy_image( source_image );
        destroy_image( rotated_image );
        return 1;
    }
    fprintf(stdout, "[+] Write image to file \n");

    destroy_image( source_image );
    destroy_image( rotated_image );

    fprintf(stdout, "[+] Free memory \n");

    fprintf(stdout, "Bye! \n");
    
    return 0;
}
