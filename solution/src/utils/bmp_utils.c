//
// Created by fk724s on 25.10.2023.
//

#include "bmp_utils.h"
#include "image_utils.h"

#include <stdio.h>

char* string_codes[] = {
        [R_OK] = "OK",
        [R_INVALID_SIGNATURE] = "Invalid signature",
        [R_INVALID_BITS] = "Invalid bits",
        [R_INVALID_HEADER] = "Invalid header",
};

enum r_status read_bmp ( FILE* file, struct image** image ) {
    struct bmp_header header;

    if ( fread( &header, sizeof header, 1, file ) != 1 ) {
        return R_INVALID_HEADER;
    }

    if ( header.bfType != BF_TYPE ) {
        return R_INVALID_SIGNATURE;
    }

    *image = create_image( header.biWidth, header.biHeight );

    if ( !*image ) {
        return R_INVALID_BITS;
    }

    uint32_t padding = padding_of(*image);

    for (uint32_t i = 0; i < header.biHeight; i++) {
        fread( &(*image)->data[i * header.biWidth], sizeof( struct pixel ), header.biWidth, file );
        fseek( file, (long) padding, SEEK_CUR );
    }

    return R_OK;
}

enum w_status write_bmp ( struct image const* image, FILE* file ) {
    struct bmp_header header = header_for( image );

    if (fwrite( &header, sizeof( header ), 1, file ) != 1) {
        return W_ERROR;
    }

    uint32_t padding = padding_of( image );

    for (uint32_t i = 0; i < ( image->height ); i++) {
        if ( fwrite(&image->data[i * image->width], sizeof( struct pixel ), image->width, file) != (image->width)) {
            return W_ERROR;
        }

        for (uint32_t j = 0; j < padding; j++) {
            if ( fputc( 0, file ) == EOF ) {
                return W_ERROR;
            }
        }
    }

    return W_OK;
}

struct bmp_header header_for ( struct image const * image ) {
    struct bmp_header header;

    uint32_t padding = padding_of( image );
    uint32_t image_size = ( image->width * sizeof( struct pixel ) + padding ) * image->height;

    header.bfType = BF_TYPE;
    header.bfileSize = image_size + sizeof( struct bmp_header );
    header.bfReserved = 0;
    header.bOffBits = sizeof( struct bmp_header );
    header.biSize = BF_SIZE;
    header.biWidth = ( image->width );
    header.biHeight = ( image->height );
    header.biPlanes = 1;
    header.biBitCount = BF_BITS;
    header.biCompression = 0;
    header.biSizeImage = image_size;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}

uint64_t padding_of ( struct image const* image ) {
    uint64_t row_size = ( image->width ) * sizeof( struct pixel );
    uint64_t padding = 0;

    if (row_size % 4 != 0) {
        padding = 4 - (row_size % 4);
    }

    return padding;
}
