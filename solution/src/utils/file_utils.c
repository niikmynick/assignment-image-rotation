//
// Created by Никита Кобик on 27.12.2023.
//

#include "file_utils.h"

FILE* open_file ( const char *  filename, enum open_mode mode ) {

    FILE *result_file = NULL;
    char * success_text;

    switch (mode) {
        case read_bytes: {
            result_file = fopen(filename, "rb");
            success_text = "[+] Open source file \n";
            break;
        }

        case write_bytes: {
            result_file = fopen(filename, "wb");
            success_text = "[+] Open destination file \n";
            break;
        }

    }

    if ( !result_file ) {
        fprintf(stderr, "FileError: an error occurred when reading file: %s \n", filename);
    } else {
        fprintf(stdout, "%s", success_text);
    }

    return result_file;
}
