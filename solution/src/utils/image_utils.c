//
// Created by fk724s on 25.10.2023.
//

#include "image_utils.h"
#include "bmp_utils.h"

#include <stdlib.h>

struct image* create_image ( const uint64_t width, const uint64_t height ) {
    struct image* image = (struct image*) malloc( sizeof( struct image ) );

    if ( !image ) {
        return NULL;
    }

    image->width = width;
    image->height = height;

    image->data = (struct pixel*) malloc( width * height * sizeof( struct pixel ) );

    if ( !(image->data) ) {
        free( image );

        return NULL;
    }

    return image;
}

void destroy_image ( struct image* image ) {
    if ( !image ) {
        return;
    }

    free( image->data );
    image->data = NULL;

    free( image );
}
