//
// Created by fk724s on 25.10.2023.
//

#ifndef IMAGE_TRANSFORMER_BMP_UTILS_H
#define IMAGE_TRANSFORMER_BMP_UTILS_H

#include <stdint.h>
#include <stdio.h>

#define BF_TYPE 19778
#define BF_SIZE 40
#define BF_BITS 24

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};


enum r_status {
    R_OK = 0,
    R_INVALID_SIGNATURE,
    R_INVALID_BITS,
    R_INVALID_HEADER
};

extern char* string_codes[];

enum w_status {
    W_OK = 0,
    W_ERROR
};

enum r_status read_bmp ( FILE* file, struct image** image );

enum w_status write_bmp ( struct image const* image, FILE* file );

struct bmp_header header_for ( struct image const * image );

uint64_t padding_of ( struct image const* image );

#endif //IMAGE_TRANSFORMER_BMP_UTILS_H
