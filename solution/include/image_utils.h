//
// Created by fk724s on 25.10.2023.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_UTILS_H
#define IMAGE_TRANSFORMER_IMAGE_UTILS_H

#include <stdint.h>

struct image* create_image ( uint64_t width, uint64_t height );

void destroy_image ( struct image* image );

#endif //IMAGE_TRANSFORMER_IMAGE_UTILS_H
