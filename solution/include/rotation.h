//
// Created by fk724s on 25.10.2023.
//

#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H

enum direction {
    LEFT = 0,
    RIGHT
};

struct image* rotate_image ( const struct image* image, enum direction direction );

#endif //IMAGE_TRANSFORMER_ROTATION_H
