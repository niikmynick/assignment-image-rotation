//
// Created by Никита Кобик on 27.12.2023.
//

#ifndef IMAGE_TRANSFORMER_FILE_UTILS_H
#define IMAGE_TRANSFORMER_FILE_UTILS_H

#include <stdio.h>

enum open_mode {
    read_bytes,
    write_bytes
};

FILE* open_file ( const char *  filename, enum open_mode mode );


#endif //IMAGE_TRANSFORMER_FILE_UTILS_H
